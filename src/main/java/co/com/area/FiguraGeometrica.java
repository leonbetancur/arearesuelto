package co.com.area;

public class FiguraGeometrica {
	private static int cantidad = 0;
	private TipoFigura tipo;
	private String nombre;
	private String comentarios;
	
	public FiguraGeometrica(TipoFigura tipo, String nombre, String comentarios) {
		this.tipo = tipo;
		this.nombre = nombre;
		this.comentarios = comentarios;
		this.cantidad++;
	}
	
	public TipoFigura getTipo() {
		return tipo;
	}

	public String getNombre() {
		return nombre;
	}

	public String getComentarios() {
		return comentarios;
	}

	public static int getCantidad() {
		return cantidad;
	}
}
