package co.com.area;

import java.util.List;

public class Operaciones {

	public double sumarAreas(List<FiguraGeometrica> listaFiguras) {
		double suma=0;
		for(FiguraGeometrica figura:listaFiguras) {
			if(figura instanceof FiguraDosDimensiones) {
				IArea area = (IArea)figura;
				suma = suma + area.calcularArea();
				
			}else {
				throw new RuntimeException("La figura "+figura.getNombre()+" no es de 2 dimensiones");
			}
			
			
		}
		return suma;
	}
}
