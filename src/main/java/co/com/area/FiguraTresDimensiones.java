package co.com.area;

public class FiguraTresDimensiones extends FiguraGeometrica {

	private int caras;
	private int aristas;
	private int vertices;
	
	public FiguraTresDimensiones( String nombre, String comentarios) {
		super(TipoFigura.TRES_DIMENSIONES, nombre, comentarios);
		
	}

	public int getCaras() {
		return caras;
	}

	public void setCaras(int caras) {
		this.caras = caras;
	}

	public int getAristas() {
		return aristas;
	}

	public void setAristas(int aristas) {
		this.aristas = aristas;
	}

	public int getVertices() {
		return vertices;
	}

	public void setVertices(int vertices) {
		this.vertices = vertices;
	}
	
	

}
