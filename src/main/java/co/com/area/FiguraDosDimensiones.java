package co.com.area;

public class FiguraDosDimensiones extends FiguraGeometrica{

	private static int cantidad = 0;
	private int lados;
	private int vertices;
	
	public FiguraDosDimensiones(String nombre, String comentarios) {
		super(TipoFigura.DOS_DIMENSIONES, nombre, comentarios);
		this.cantidad++;
	}
	public int getLados() {
		return lados;
	}
	public void setLados(int lados) {
		this.lados = lados;
	}
	public int getVertices() {
		return vertices;
	}
	public void setVertices(int vertices) {
		this.vertices = vertices;
	}
}
