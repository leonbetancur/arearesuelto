package co.com.area;

import java.util.ArrayList;
import java.util.List;

public class Principal {

	public static void main(String[] args) {
		
		List<FiguraGeometrica> listaFiguras = new ArrayList<>();
		
		System.out.println("Iniciado..");
		Triangulo triangulo1 = new Triangulo(10.1, 5.2);
		Triangulo triangulo2 = new Triangulo(2.1, 5.2);
		TrianguloEquilatero trianguloEquilatero1 = new TrianguloEquilatero(3.2);
		TrianguloEquilatero trianguloEquilatero2 = new TrianguloEquilatero(4.7);
		TrianguloEquilatero trianguloEquilatero3 = new TrianguloEquilatero(5.3);
		
		listaFiguras.add(triangulo1);
		listaFiguras.add(triangulo2);
		listaFiguras.add(trianguloEquilatero1);
		listaFiguras.add(trianguloEquilatero2);
		listaFiguras.add(trianguloEquilatero3);
		
		System.out.println("Cantidad figuras Geometricas: "+FiguraGeometrica.getCantidad());
		System.out.println("Cantidad Triangulos: "+triangulo1.getCantidad());
		System.out.println("Cantidad Triangulos Equilateros: "+trianguloEquilatero2.getCantidadInstancias());
		
		System.out.println("Areas:");
		System.out.println(triangulo1.calcularArea());
		System.out.println(triangulo2.calcularArea());
		System.out.println(trianguloEquilatero1.calcularArea());
		System.out.println(trianguloEquilatero2.calcularArea());
		System.out.println(trianguloEquilatero3.calcularArea());
		
		Operaciones operaciones = new Operaciones();
		
		System.out.println("Suma areas: "+operaciones.sumarAreas(listaFiguras));
		
		Cubo cubo1 = new Cubo(3.23);
		listaFiguras.add(cubo1);
		
		System.out.println("Suma areas: "+operaciones.sumarAreas(listaFiguras));
	}
	

}
