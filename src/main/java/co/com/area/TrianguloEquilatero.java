package co.com.area;

public class TrianguloEquilatero extends FiguraDosDimensiones implements IArea, IPerimetro {
	private static int cantidad = 0;
	private double lado;
	
	public TrianguloEquilatero(double lado) {
		super("Tr�angulo Equil�tero", "a es la longitud de un lado");
		this.lado = lado;
		this.cantidad++;
	}
	
	@Override
	public double calcularPerimetro() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public double calcularArea() {
		// TODO Auto-generated method stub
		return (Math.sqrt(3)/4)*(Math.pow(lado, 2));
	}
	
	public int getCantidadInstancias() {
		return cantidad;
	}

}
