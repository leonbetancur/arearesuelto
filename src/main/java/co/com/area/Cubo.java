package co.com.area;

public class Cubo extends FiguraTresDimensiones implements IVolumen {

	
	private double longitudArista; 
	
	public Cubo(double longitudArista) {
		super("Cubo", "a es la longitud de la arista");
		this.longitudArista = longitudArista;
	}

	@Override
	public double calcularVolumen() {
		return longitudArista*longitudArista*longitudArista;
	}

}
