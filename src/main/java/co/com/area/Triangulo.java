package co.com.area;

public class Triangulo extends FiguraDosDimensiones implements IArea, IPerimetro {

	private static int cantidad = 0;
	private double base;
	private double altura;

	
	public Triangulo(double altura, double base) {
		super("Tr�angulo", "b es la longitud de la base, h la altura, a y c la longitud de los otros dos lados");
		this.altura = altura;
		this.base = base;
		this.cantidad++;
	}

	
	public double getBase() {
		return base;
	}

	public void setBase(double base) {
		this.base = base;
	}

	public double getAltura() {
		return altura;
	}

	public void setAltura(double altura) {
		this.altura = altura;
	}

	@Override
	public double calcularArea() {
		return (base+altura)/2;
	}

	@Override
	public double calcularPerimetro() {
		return 0;
	}
	
	public static int getCantidad() {
		return cantidad;
	}

}
