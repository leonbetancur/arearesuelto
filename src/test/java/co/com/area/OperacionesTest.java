package co.com.area;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;


public class OperacionesTest {

	private Operaciones operaciones;
	
	@Before
	public void iniciarTest() {
		operaciones = new Operaciones();
	}
	
	@Test
	public void testSumarAreas() {
		
		//Arrange
		FiguraGeometrica figura1 = new Triangulo(20, 30);
		FiguraDosDimensiones figura2 = new TrianguloEquilatero(10);
		Triangulo figura3 = new Triangulo(5,4);
		TrianguloEquilatero figura4 = new TrianguloEquilatero(5);
		
		List<FiguraGeometrica>  listaFiguras = new ArrayList<>();
		listaFiguras.add(figura1);
		listaFiguras.add(figura2);
		listaFiguras.add(figura3);
		listaFiguras.add(figura4);
		
		//Act
		double suma = operaciones.sumarAreas(listaFiguras);
		
		//Assert
		assertEquals(83.62658773652741, suma, 0.001);
	}
	
	@Test(expected=RuntimeException.class)
	public void testDeberiaFallarSumarAreasExpected() {
		
		//Arrange
		FiguraGeometrica figura1 = new Cubo(4);
		FiguraDosDimensiones figura2 = new TrianguloEquilatero(10);
		Triangulo figura3 = new Triangulo(5,4);
		TrianguloEquilatero figura4 = new TrianguloEquilatero(5);
		
		List<FiguraGeometrica>  listaFiguras = new ArrayList<>();
		listaFiguras.add(figura1);
		listaFiguras.add(figura2);
		listaFiguras.add(figura3);
		listaFiguras.add(figura4);
		
		//Act
		double suma = operaciones.sumarAreas(listaFiguras);
		
		//Assert
		assertEquals(83.62658773652741, suma, 0.001);
	}	
	
	@Test
	public void testDeberiaFallarSumarAreasTryCatch() {
		
		//Arrange
		FiguraGeometrica figura1 =  new Cubo(4);
		FiguraDosDimensiones figura2 = new TrianguloEquilatero(10);
		Triangulo figura3 = new Triangulo(5,4);
		TrianguloEquilatero figura4 = new TrianguloEquilatero(5);
		
		List<FiguraGeometrica>  listaFiguras = new ArrayList<>();
		listaFiguras.add(figura1);
		listaFiguras.add(figura2);
		listaFiguras.add(figura3);
		listaFiguras.add(figura4);
		
		//Act
		double suma;
		String mensajeError="";
		try {
			suma = operaciones.sumarAreas(listaFiguras);
		}catch(RuntimeException re){
			mensajeError = re.getMessage();
		}
		
		
		//Assert
		assertEquals("La figura Cubo no es de 2 dimensiones", mensajeError);
	}	

	
	
}
